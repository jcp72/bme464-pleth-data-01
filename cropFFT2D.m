function [F, A] = cropFFT2D(f, a, fmin, fmax)
    
    minFreqIndex = find(f <= fmin, 1);
    maxFreqIndex = find(f > fmax, 1);
    
    if isempty(maxFreqIndex)
        maxFreqIndex = length(f);
    end

    F = f(minFreqIndex:maxFreqIndex);
    A = a(minFreqIndex:maxFreqIndex, :);
end