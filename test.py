import numpy as np
import matplotlib.pyplot as plt
from statistics import NormalDist

def main():
    with open("DEMO.TXT", "r") as file:
        data = np.array([int(v) for v in file.read().splitlines()])

    smooth_10Hz_data = np.array([
        np.average(data[
            i * 10 :
            (i + 1) * 10 + 10
        ]) for i in range(len(data) // 10)
    ])

    n = 30
    moving_average_3_seconds = np.array([
        np.average(smooth_10Hz_data[
            max(0, i - n // 2) :
            min(len(smooth_10Hz_data), i + n // 2)
        ]) for i in range(len(smooth_10Hz_data))
    ])

    # plt.plot(smooth_10Hz_data - moving_average_3_seconds)

    final_smoothed = smooth_10Hz_data - moving_average_3_seconds

    f, t, a = windowedFFT(final_smoothed, 10, 200, 30)
    f, a = cropFFT2D(f, a, 0, 1)
    # plt.imshow(a, origin='lower')
    S = getStrongestFrequencies(f, a) * 60
    n = 10
    A = [np.average(S[max(0, i - n//2) : min(len(S), i + n // 2)]) for i in range(len(S))]

    timeSums = np.log(np.max(a, 0))
    SD = np.std(timeSums)
    dist = NormalDist(mu = np.average(timeSums) - 2 * SD, sigma = SD)
    C = [1 - dist.cdf(ts) for ts in timeSums]
    
    plt.plot(A)
    plt.xlabel("Time (s)")
    plt.ylabel("Breathing rate per minute")
    ax2 = plt.gca().twinx()
    ax2.plot(C, c="C1", linestyle="--")
    ax2.set_ylabel("Confidence of apnea event")
    plt.show()

################################################################################

def windowedFFT(sample, fs, winSize, upscalingFactor = 1):

    length = len(sample)
    if winSize > length:
        raise ValueError("Window is larger than sample!")
    
    numWindows = (length - winSize) // upscalingFactor

    T = np.array([i for i in range(numWindows)]) * upscalingFactor / fs
    F = np.array([i for i in range(winSize // 2 + 1)]) * fs / winSize

    numFreqs = len(F)

    A = np.zeros([numFreqs, numWindows])

    for t in range(numWindows):
        f, a = analyzeFreq(sample[t * upscalingFactor : t * upscalingFactor + winSize], fs)
        A[:, t] = a

    return F, T, A

def cropFFT2D(f, a, fmin, fmax):

    minFreqIndex = np.argmax(f <= fmin) 
    maxFreqIndex = np.argmax(f > fmax)

    if fmax > max(f):
        maxFreqIndex = len(f)

    return f[minFreqIndex:maxFreqIndex], a[minFreqIndex:maxFreqIndex,:]

def getStrongestFrequencies(f, a):
    numFreqs, numWindows = a.shape
    F = np.zeros([numWindows])
    for i in range(numWindows):
        windowFT = a[:, i]
        strongestFreq = f[np.argmax(windowFT)]
        F[i] = strongestFreq

    return F

def analyzeFreq(sample, fs):
    S = np.fft.fft(sample)
    L = len(sample)
    twoSided = abs(S / L)

    F = np.array([i for i in range(0, L//2 + 1)]) * fs / L
    A = twoSided[0:L//2 + 1]
    A[2:-1] *= 2
    return F, A

if __name__ == "__main__":
    main()
