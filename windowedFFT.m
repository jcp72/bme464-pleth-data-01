
function [F, T, A] = windowedFFT(sample, fs, winSize, upscalingFactor)
% windowedFFT - calculate the Fourier transform at regular intervals of a
% sample
%
%
%

    if nargin <= 3
        upscalingFactor = 1; % make this number > 1 to only calculate windowed FFT every <upscalingFactor> samples
    end
    
    len = length(sample);
    if winSize > len
        error("Window is larger than sample!")
    end

    
    numWindows = floor((len - winSize) / upscalingFactor);
    
    T = (0:(numWindows - 1)) / fs * upscalingFactor;
    F = fs * (0:(winSize / 2)) / winSize;
    
    numFreqs = length(F);
    
    A = zeros(numFreqs, numWindows);
    
    for t = 1:numWindows
        [f, a] = analyzeFreq(sample(t * upscalingFactor:t * upscalingFactor + winSize - 1), fs);
        A(:, t) = a';
    end
    
end