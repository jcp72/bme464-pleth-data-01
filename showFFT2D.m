

function [] = showFFT2D(F, T, A, Y)

    if nargin < 4
        Y = "Frequency (Hz)";
    end

    imagesc(T, F, A);
    
    set(gca,'YDir','normal')
    xlabel("Time (s)");
    ylabel(Y);
    
end