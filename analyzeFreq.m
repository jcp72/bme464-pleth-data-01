function [F, A] = analyzeFreq(sample, fs)
    % https://www.mathworks.com/help/matlab/ref/fft.html
    S = fft(sample);
    L = length(sample);
    twoSided = abs(S / L);
    
    F = fs * (0:(L / 2)) / L;
    A = twoSided(1:L / 2 + 1); % Use the zero and positive regions of the FFT result: S(1) and S(2)...S(L/2 + 1)
    A(2:end - 1) = 2 * A(2:end - 1); % Recover original amplitudes
end
