function F = getStrongestFrequencies(f, a)
    [numFreqs, numWindows] = size(a);
    F = zeros(1, numFreqs);
    for i = 1:numWindows
        windowFT = a(:, i);
        strongestFreq = f(find(windowFT == max(windowFT), 1));
        F(i) = strongestFreq;
    end
end