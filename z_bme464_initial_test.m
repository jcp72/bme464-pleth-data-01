V_cell = textscan(fopen("DEMO.TXT"), "%d");
Data = V_cell{:};

V = double(Data)';
% V = Data(1:12000);
% V = Data(14000:end);

SHOW_DIAGNOSTIC = 0;

Fs = 100;

AVERAGE_N_SAMPLES = 1;

N = 1;

if AVERAGE_N_SAMPLES
    % take the average of N samples
    N = 10;
    K = arrayfun(@(i) mean(V(1+N*(i-1) : N*i)), 1:floor(length(V)/N));
    V = K;
    Fs = Fs / N;
end

if 1
    % find moving average
    n = 300 / N;
    Avg = arrayfun(@(i) mean(V(max(1, i-n/2) : min(length(V), i+n/2))), 1:length(V));
end

% windowedFFT (data, Fs, windowSize, upscalingFactor)
windowSizeSeconds = 20;

if SHOW_DIAGNOSTIC, ax1 = subplot(5, 1, 1); end
X = (1:length(V))./Fs - windowSizeSeconds / 2;
% plot(X, V);
% hold on
% plot(X, Avg)
% hold off
plot(X, V - Avg)

[f, t, a] = windowedFFT(V - Avg, Fs, windowSizeSeconds * Fs, 30);
[f, a] = cropFFT2D(f, a, 0, 1);
if SHOW_DIAGNOSTIC, ax2 = subplot(5, 1, [2, 3, 4]); end
showFFT2D(f * 60, t, a, "Breathing rate per minute")

S = getStrongestFrequencies(f, a);
n = 100 / N;
A = arrayfun(@(i) mean(S(max(1, i - n/2): min(length(S), i + n/2))), 1:length(S));
hold on
plot(t, 60*S, "color", "red");
plot(t, 60*A, "color", "white");
hold off

% timeSums = sum(a .^ 2, 1);
% timeSums = log(sum(a, 1));
timeSums = log(max(a, [], 1));
threshold_low = mean(timeSums) - 2 * std(timeSums);
% timeSums = sum(log(a), 1);
if SHOW_DIAGNOSTIC, ax3 = subplot(5, 1, 5); end
plot(t, timeSums)
hold on
plot(t, repmat(threshold_low, 1, length(t)))
hold off

if SHOW_DIAGNOSTIC, linkaxes([ax1, ax2, ax3], 'x'), end
% maximize window
% set(gcf, 'Position', get(0, 'Screensize'));

if SHOW_DIAGNOSTIC
    % saveas(gca, "z_bme464_matlab.png")
    return
end

clf

sd = std(timeSums);
mu = mean(timeSums);
C = 1 - normcdf(timeSums, mu - 2 * sd, sd);

ax1 = subplot(3, 1, 1);
plot(X, V - Avg)
ylabel("Signal")

ax2 = subplot(3, 1, [2, 3]);
yyaxis left
plot(t, 60*A, "LineWidth", 2)
xlabel("Time (s)")
ylabel("Breathing rate per minute")
hold on
yyaxis right
ylabel("Confidence of apnea event")
plot(t, C, "--", "LineWidth", 2)
hold off

linkaxes([ax1, ax2], 'x')

